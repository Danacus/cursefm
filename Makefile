CC=gcc
CFLAGS=`pkg-config --cflags ncurses`
LDFLAGS=`pkg-config --libs ncurses`
FLAGS_EXTRA=``

all: clean run

main: main.c
	$(CC) -o main main.c $(CFLAGS) $(LDFLAGS) $(FLAGS_EXTRA)

run: main
	./main

clean:
	rm -f main
	rm -rf *.o
